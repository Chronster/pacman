#pragma once

#include "precomp.h"
#include "Field.h"
#include "ACharacter.h"
#include "pacman/MainCharacter.h"

class Score
{
public:
	Score(Canvas gc, Field *field, MainCharacter *pacman);
	~Score(void);
	void DrawScore();
	void Add(int points);
	unsigned int* GetScore();

private:
	unsigned int m_Score;
	Canvas m_Gc;
	Font m_StandardFont;
	Field *m_Field;
	MainCharacter *m_Pacman;
	Sprite *m_LifeSprite;
};