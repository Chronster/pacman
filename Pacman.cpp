#include "Pacman.h"
#include "precomp.h"

float timer = 0.0f;
float fps_timer = 0.0f;
#define FPS_LIMIT 100

Pacman::Pacman(void) : m_Field(NULL), m_Pacman(NULL), m_Shadow(NULL), m_Speedy(NULL), m_Bashful(NULL), m_Pokey(NULL)
{
	round = 0;
}

Pacman::~Pacman(void)
{
	delete m_Field;
	delete m_Score;
	delete m_Pacman;
	delete m_Shadow;
	delete m_Speedy;
	delete m_Bashful;
	delete m_Pokey;
	for (int i=0; i<(sizeof(resArray)/sizeof(resArray[0])); i++) 
	{
		delete resArray[i];
	}
	delete m_Endscene;
}

int Pacman::start(const std::vector<string> &args)
{
	quit = false;

	DisplayWindowDescription desc;
	desc.set_title("Pacman");
	desc.set_size(Size(450, 500+16), true);
	desc.set_allow_resize(true);

	DisplayWindow window(desc);
	InputContext ic = window.get_ic();
	InputDevice keyboard = ic.get_keyboard();

	Slot slot_quit = window.sig_window_close().connect(this, &Pacman::on_window_close);
	Slot slot_input_up = (window.get_ic().get_keyboard()).sig_key_up().connect(this, &Pacman::on_input_up);
	Canvas gc(window);

	int last_time = static_cast<int>(System::get_time());

	ResourceManager resources = XMLResourceManager::create(XMLResourceDocument("resources/resources.xml"));

	m_Field = new Field(gc, resources);

	resArray[0] = new Sprite;
	resArray[1] = new Sprite;
	resArray[2] = new Sprite;
	resArray[3] = new Sprite;
	resArray[4] = new Sprite;

	*resArray[0] = Sprite::resource(gc, "pacman_up", resources);
	*resArray[1] = Sprite::resource(gc, "pacman_right", resources);
	*resArray[2] = Sprite::resource(gc, "pacman_down", resources);
	*resArray[3] = Sprite::resource(gc, "pacman_left", resources);
	*resArray[4] = Sprite::resource(gc, "pacman_dying", resources);

	m_Pacman = new MainCharacter(resArray[0], resArray[1], resArray[2], resArray[3], resArray[4], keyboard, m_Field);
	m_Score = new Score(gc, m_Field, m_Pacman);

	m_Shadow = new Shadow(m_Pacman, m_Field, 11, 13, 24, -2);
	m_Speedy = new Speedy(m_Pacman, m_Field, 13, 13, 2, -2);
	m_Bashful = new Bashful(m_Pacman, m_Shadow, m_Field, 12, 13, 27, 32);
	m_Pokey = new Pokey(m_Pacman, m_Field, 13, 13, 2, 32);
	m_Endscene = new Endscene(gc, (int*)m_Score->GetScore());

	while (!quit)
	{
		if((m_Pacman->GetLifes() <= 0 && m_Pacman->HasDeathAnimationEnded()) || m_Field->GetDotsEaten() >= 242) 
		{
			gc.clear(Colorf(0.0f,0.0f,0.0f));
			m_Endscene->Render();
		}

		else 
		{
			int current_time = static_cast<int>(System::get_time());
			float time_delta_ms = static_cast<float>(current_time - last_time);
			timer += time_delta_ms / 1000.0f;
			fps_timer += time_delta_ms / 1000.0f;
			last_time = current_time;

			if((timer > 5.0f) && (round % 2 == 0)) //5 sekunden weglaufen / patrollieren 20 sekunden verfolgen
			{
				timer = 0.0f;
				round++;
				static_cast<BaseGhost*>(m_Shadow)->SetState(BaseGhost::EGhostState::Persuit);
				static_cast<BaseGhost*>(m_Speedy)->SetState(BaseGhost::EGhostState::Persuit);
				static_cast<BaseGhost*>(m_Bashful)->SetState(BaseGhost::EGhostState::Persuit);
				static_cast<BaseGhost*>(m_Pokey)->SetState(BaseGhost::EGhostState::Persuit);
			}
			else if((timer > 20.0f) && (round % 2 == 1)) // danach patrollieren
			{
				timer = 0.0f;
				round++;
				static_cast<BaseGhost*>(m_Shadow)->SetState(BaseGhost::EGhostState::Patrol);
				static_cast<BaseGhost*>(m_Speedy)->SetState(BaseGhost::EGhostState::Patrol);
				static_cast<BaseGhost*>(m_Bashful)->SetState(BaseGhost::EGhostState::Patrol);
				static_cast<BaseGhost*>(m_Pokey)->SetState(BaseGhost::EGhostState::Patrol);
			}

			if(m_Field->GetBigDotEaten())
			{
				static_cast<BaseGhost*>(m_Shadow)->SetState(BaseGhost::EGhostState::Frightened);
				static_cast<BaseGhost*>(m_Speedy)->SetState(BaseGhost::EGhostState::Frightened);
				static_cast<BaseGhost*>(m_Bashful)->SetState(BaseGhost::EGhostState::Frightened);
				static_cast<BaseGhost*>(m_Pokey)->SetState(BaseGhost::EGhostState::Frightened);
			}

			if(fps_timer > (1.0f / FPS_LIMIT))
			{
				fps_timer = 0.0f;
				m_Pacman->Update();
				m_Shadow->Update();
				m_Speedy->Update();
				m_Bashful->Update();
				m_Pokey->Update();

				if(!m_Pacman->isAlive())
				{
					if(m_Pacman->HasDeathAnimationEnded())
					{
						m_Pacman->ResetPosition();
						m_Shadow->ResetPosition();
						m_Speedy->ResetPosition();
						m_Bashful->ResetPosition();
						m_Pokey->ResetPosition();
					}
				}
			}

			gc.clear(Colorf(0.0f,0.0f,0.0f));

			m_Field->DrawField();
			m_Score->DrawScore();
			m_Pacman->Render(gc);
			m_Shadow->Render(gc);
			m_Speedy->Render(gc);
			m_Bashful->Render(gc);
			m_Pokey->Render(gc);
		}

		window.flip(1);
		KeepAlive::process(0);
	}

	return 0;
}

void Pacman::on_input_up(const InputEvent &key)
{
	if(key.id == keycode_escape)
	{
		quit = true;
	}
}

void Pacman::on_window_close()
{
	quit = true;
}