#include "Field.h"
#include <fstream>
#include <cstring>

Field::Field(Canvas gc, ResourceManager resources)
{
	m_Gc = gc;
	m_Resources = resources;
	m_DotsEaten = 0;
	m_BigDotsEaten = 0;
	m_BigDotEaten = false;
	LoadField();
}

Field::~Field(void)
{
	for (unsigned short i = 0; i < m_Field.size(); ++i)
	{
		delete m_Field[i];
	}
}

void Field::LoadField()
{
	ifstream stream("resources/Level1.txt");

	while(!stream.eof())
	{
		char* line = new char[50];
		stream.getline(line, 50);
		m_Field.push_back(line);		
	}

	m_SizeX = strlen(m_Field[0]);
	m_SizeY = m_Field.size();
	
	// load all sprites
	m_FieldTiles.insert(make_pair('1', Sprite::resource(m_Gc, "outerwall_top", m_Resources)));
	m_FieldTiles.insert(make_pair('2', Sprite::resource(m_Gc, "outerwall_right", m_Resources)));
	m_FieldTiles.insert(make_pair('3', Sprite::resource(m_Gc, "outerwall_bottom", m_Resources)));
	m_FieldTiles.insert(make_pair('4', Sprite::resource(m_Gc, "outerwall_left", m_Resources)));
	m_FieldTiles.insert(make_pair('5', Sprite::resource(m_Gc, "outercorner_top_left", m_Resources)));
	m_FieldTiles.insert(make_pair('6', Sprite::resource(m_Gc, "outercorner_top_right", m_Resources)));
	m_FieldTiles.insert(make_pair('7', Sprite::resource(m_Gc, "outercorner_bottom_right", m_Resources)));
	m_FieldTiles.insert(make_pair('8', Sprite::resource(m_Gc, "outercorner_bottom_left", m_Resources)));
	m_FieldTiles.insert(make_pair('(', Sprite::resource(m_Gc, "outercorner_narrow_top_left", m_Resources)));
	m_FieldTiles.insert(make_pair(')', Sprite::resource(m_Gc, "outercorner_narrow_top_right", m_Resources)));
	m_FieldTiles.insert(make_pair(']', Sprite::resource(m_Gc, "outercorner_narrow_bottom_right", m_Resources)));
	m_FieldTiles.insert(make_pair('[', Sprite::resource(m_Gc, "outercorner_narrow_bottom_left", m_Resources)));
	m_FieldTiles.insert(make_pair('y', Sprite::resource(m_Gc, "gate_left", m_Resources)));
	m_FieldTiles.insert(make_pair('z', Sprite::resource(m_Gc, "gate_right", m_Resources)));
	m_FieldTiles.insert(make_pair('-', Sprite::resource(m_Gc, "door", m_Resources)));
	m_FieldTiles.insert(make_pair('A', Sprite::resource(m_Gc, "wall_top", m_Resources)));
	m_FieldTiles.insert(make_pair('B', Sprite::resource(m_Gc, "wall_right", m_Resources)));
	m_FieldTiles.insert(make_pair('C', Sprite::resource(m_Gc, "wall_bottom", m_Resources)));
	m_FieldTiles.insert(make_pair('D', Sprite::resource(m_Gc, "wall_left", m_Resources)));
	m_FieldTiles.insert(make_pair('E', Sprite::resource(m_Gc, "corner_top_left", m_Resources)));
	m_FieldTiles.insert(make_pair('F', Sprite::resource(m_Gc, "corner_top_right", m_Resources)));
	m_FieldTiles.insert(make_pair('H', Sprite::resource(m_Gc, "corner_bottom_right", m_Resources)));
	m_FieldTiles.insert(make_pair('G', Sprite::resource(m_Gc, "corner_bottom_left", m_Resources)));
	m_FieldTiles.insert(make_pair('a', Sprite::resource(m_Gc, "outernose_top_left", m_Resources)));
	m_FieldTiles.insert(make_pair('b', Sprite::resource(m_Gc, "outernose_top_right", m_Resources)));
	m_FieldTiles.insert(make_pair('c', Sprite::resource(m_Gc, "outernose_right_top", m_Resources)));
	m_FieldTiles.insert(make_pair('d', Sprite::resource(m_Gc, "outernose_right_bottom", m_Resources)));
	m_FieldTiles.insert(make_pair('e', Sprite::resource(m_Gc, "outernose_bottom_left", m_Resources)));
	m_FieldTiles.insert(make_pair('f', Sprite::resource(m_Gc, "outernose_bottom_right", m_Resources)));
	m_FieldTiles.insert(make_pair('g', Sprite::resource(m_Gc, "outernose_left_top", m_Resources)));
	m_FieldTiles.insert(make_pair('h', Sprite::resource(m_Gc, "outernose_left_bottom", m_Resources)));
	m_FieldTiles.insert(make_pair('.', Sprite::resource(m_Gc, "dot", m_Resources)));
	m_FieldTiles.insert(make_pair('*', Sprite::resource(m_Gc, "big_dot", m_Resources)));
	
}

void Field::DrawField()
{
	float scaleX = 0.16f;
	float scaleY = 0.16f;

	for(unsigned short i = 0; i < m_Field.size(); i++)
	{
		char* line = m_Field[i];
		
		int j = 0;

		while(line[j] != '\n')
		{
			char element = line[j];
			++j;
			
			if(element != ' ' && m_FieldTiles.find(element) != m_FieldTiles.end())
			{
				Sprite *buffer = &m_FieldTiles.find(element)->second;

				buffer->set_scale(scaleX, scaleY);
				buffer->update(10);
				buffer->draw(m_Gc, (j - 1) * (buffer->get_width() * scaleX), i * (buffer->get_height() * scaleY));
			}
			else if(element != ' ' && m_FieldTiles.find(element) == m_FieldTiles.end())
			{
				break;
			}
		}
	}
}

bool Field::IsBlocked(const int x, const int y) const
{
	if(x < 0 || y < 0 || x >= (int)strlen(m_Field[x]) || y >= (int)m_Field.size())
	{
		return false;
	}

	char c = m_Field[y][x];

	if(c == ' ' || c == '.' || c == '*')
	{
		return false;
	}

	return true;
}

bool Field::IsBlockedForGhost(const  int x, const  int y) const
{
	bool blocked = IsBlocked(x, y);

	if(blocked && m_Field[y][x] == '-')
	{
		return false;
	}

	return blocked;
}

char Field::GetField(const  int x, const  int y) const
{
	return m_Field[y][x];
}

void Field::EatField(const  int x, const  int y)
{
	if(m_Field[y][x] == '*')
	{
		m_BigDotEaten = true;
		m_Field[y][x] = ' ';
		++m_BigDotsEaten;
	}

	if(m_Field[y][x] == '.')
	{
		m_Field[y][x] = ' ';
		++m_DotsEaten;
	}
}

bool Field::IsCrossing(const  int x, const  int y) const
{
	 short directions = 0;

	if(!IsBlockedForGhost(x + 1, y))
	{
		++directions;
	}

	if(!IsBlockedForGhost(x - 1, y))
	{
		++directions;
	}

	if(!IsBlockedForGhost(x, y + 1))
	{
		++directions;
	}

	if(!IsBlockedForGhost(x, y - 1))
	{
		++directions;
	}

	return directions > 2;
}

bool Field::IsEdge(const  int x, const  int y) const
{
	if(!IsBlockedForGhost(x + 1, y) && !IsBlockedForGhost(x, y + 1))
	{
		return true;
	}

	if(!IsBlockedForGhost(x - 1, y) && !IsBlockedForGhost(x, y + 1))
	{
		return true;
	}

	if(!IsBlockedForGhost(x + 1, y) && !IsBlockedForGhost(x, y - 1))
	{
		return true;
	}

	if(!IsBlockedForGhost(x - 1, y) && !IsBlockedForGhost(x, y - 1))
	{
		return true;
	}

	return false;
}

 int Field::GetSizeX() const
{
	return m_SizeX;
}

int Field::GetSizeY() const
{
	return m_SizeY;
}

bool Field::GetBigDotEaten()
{
	if(m_BigDotEaten)
	{
		m_BigDotEaten = false;
		return true;
	}

	return false;
}

unsigned int Field::GetDotsEaten() const
{
	return m_DotsEaten;
}

unsigned int Field::GetBigDotsEaten() const
{
	return m_BigDotsEaten;
}

bool Field::InHouse(const int x, const int y) const
{
	return (x > 10 && x < 17) && (y > 12 && y < 16);
}