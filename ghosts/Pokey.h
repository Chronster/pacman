#pragma once

#include <math.h>

#include "BaseGhost.h"

class Pokey : public BaseGhost
{
public:
	Pokey(MainCharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY);
	~Pokey(void);
	
	virtual void Render(Canvas gc);
	virtual void Chase(int pacmanX, int pacmanY);
};

