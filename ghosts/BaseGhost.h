#pragma once
#include <math.h>
#include <time.h>
#include <random>

#include "../ACharacter.h"
#include "../pacman/MainCharacter.h"

class BaseGhost : public ACharacter
{

private:
	Sprite* m_EatableSprite;
	Sprite* m_EyesSprite;
	int m_StartTime;

public:
	enum EGhostState
	{
		Patrol = 0,
		Persuit = 1,
		Frightened = 2
	};

	BaseGhost(void);
	BaseGhost(MainCharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY);
	~BaseGhost(void);

	virtual void Move();
	virtual void Render(Canvas gc) = 0;
	virtual void Chase(int pacmanX, int pacmanY) = 0;
	void Escape();
	void SetTarget( int targetX,  int targetY);
	void SetState(EGhostState state);
	void ResetPosition();

protected:
	bool m_Eatable;
	short m_CornerX;		// the home corner of the ghost
	short m_CornerY;
	short m_StartPosX;
	short m_StartPosY;
	short m_TargetX;		// the current target of the ghost
	short m_TargetY;
	EGhostState m_CurrentState;
	EDirectionType m_LastDecision;
	virtual void RenderEyes(Canvas gc);
	void MoveToTarget();
	void MoveToTargetPersueMode(int posX, int posY);
	bool MoveHorizontal(bool left);
	bool MoveVertical(bool up);
	MainCharacter* m_Pacman;
};