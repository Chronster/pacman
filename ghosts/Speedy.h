#pragma once

#include "BaseGhost.h"

class Speedy : public BaseGhost
{
public:
	Speedy(MainCharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY);
	~Speedy(void);

	virtual void Render(Canvas gc);
	virtual void Chase(int pacmanX, int pacmanY);
};