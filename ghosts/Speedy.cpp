//Speedy = pink
//� Nickname: Pinky
//� When Speedy isn't patrolling the northwest
//corner, Pinky tries to attack Pac-Man by moving to
//where he is going to be (that is, a few spaces
//ahead of Pac-Man's current direction) instead of
//right where he is, as Blinky does.
//� When the ghosts are not patrolling their home
//corners, Pinky wants to go to the place that is four
//grid spaces ahead of Pac-Man in the direction that
//Pac-Man is facing.
//� If Pac-Man is facing up, Pinky wants to go to the
//location exactly four spaces above Pac-Man.
//He does this following the same logic that Blinky
//uses to find Pac-Man's exact location.

#include "Speedy.h"

Speedy::Speedy(MainCharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY) : BaseGhost(pacman, field, xPos, yPos, cornerX, cornerY)
{
}

Speedy::~Speedy(void)
{
	delete m_Sprite;
}

void Speedy::Render(Canvas gc)
{
	if(!m_Eatable)
	{
		if(!m_Sprite)
		{
			ResourceManager resources = XMLResourceManager::create(XMLResourceDocument("resources/resources.xml"));
			m_Sprite = new Sprite();
			*m_Sprite = Sprite::resource(gc, "speedy", resources);
		}

		if (m_Alive == true) 
		{
			m_Sprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
			m_Sprite->update(10);
		}

		BaseGhost::RenderEyes(gc);
	}
	else
	{
		BaseGhost::Render(gc);
	}
}

void Speedy::Chase(int pacmanX, int pacmanY)
{
	EDirectionType lookDirection = m_Pacman->GetViewDirection();
	if (lookDirection == Up)
	{
		MoveToTargetPersueMode(pacmanX, pacmanY-4);
	} 
	else if (lookDirection == Right)
	{
		MoveToTargetPersueMode(pacmanX+4, pacmanY);
	}
	else if (lookDirection == Down)
	{
		MoveToTargetPersueMode(pacmanX, pacmanY+4);
	}
	else if (lookDirection == Left)
	{
		MoveToTargetPersueMode(pacmanX-4, pacmanY);
	}
}