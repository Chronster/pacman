//Shadow = rot
//� Nickname: Blinky
//� As his name implies, Shadow is usually a constant
//shadow on Pac-Man's tail. When he's not patrolling
//the northeast corner of the maze, Shadow tries to
//find the quickest route to Pac-Man's position.
//� When the ghosts are not patrolling their home
//corners, Blinky will attempt to shorten the distance
//between Pac-Man and himself.
//� If he has to choose between shortening the
//horizontal or vertical distance, he will choose to
//shorten whichever is greatest. For example, if
//Pac-Man is 4 grid spaces to the left, and 7 grid
//space above Blinky, Blinky will try to move up
//before he moves to the left.

#include "Shadow.h"

Shadow::Shadow(MainCharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY) : BaseGhost(pacman, field, xPos, yPos, cornerX, cornerY)
{
	/*
	m_PosX = xPos;
	m_PosY = yPos;
	m_PixelPosX = xPos * 32;
	m_PixelPosY = yPos * 32;
	m_PixelPosX = xPos * m_Sprite->get_width();
	m_PixelPosY = yPos * m_Sprite->get_height();*/
}

Shadow::~Shadow(void)
{
	delete m_Sprite;
}

void Shadow::Render(Canvas gc)
{
	if(!m_Eatable)
	{
		if(!m_Sprite)
		{
			ResourceManager resources = XMLResourceManager::create(XMLResourceDocument("resources/resources.xml"));
			m_Sprite = new Sprite();
			*m_Sprite = Sprite::resource(gc, "shadow", resources);
		}

		if (m_Alive == true) 
		{
			m_Sprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
			m_Sprite->update(10);
		}

		BaseGhost::RenderEyes(gc);
	}
	else
	{
		BaseGhost::Render(gc);
	}
}

void Shadow::Chase(int pacmanX, int pacmanY)
{
	MoveToTargetPersueMode(pacmanX, pacmanY);
	/*int oppositeDir = 2;
	float deltaX = pacmanX - m_PosX;
	float deltaY = pacmanY - m_PosY;
	//EDirectionType primary, secondary, third, fourth;
	EDirectionType dirOrder[4];
	Vec2<int> dirVecOrder[4];

	Vec2<int> up, left, down, right;
	up = Vec2<int>(m_PosX, m_PosY-1);
	left = Vec2<int>(m_PosX-1, m_PosY);
	down = Vec2<int>(m_PosX, m_PosY+1);
	right = Vec2<int>(m_PosX+1, m_PosY);


	if (abs(deltaX) >= abs(deltaY))
	{
		if (deltaX <= 0 && deltaY <= 0) 
		{
			dirOrder[0] = Left;
			dirOrder[1] = Up;
			dirOrder[2] = Right;
			dirOrder[3] = Down;

			dirVecOrder[0] = left;
			dirVecOrder[1] = up;
			dirVecOrder[2] = right;
			dirVecOrder[3] = down;
		} 
		else if (deltaX <= 0 && deltaY > 0)
		{
			dirOrder[0] = Left;
			dirOrder[1] = Down;
			dirOrder[2] = Right;
			dirOrder[3] = Up;

			dirVecOrder[0] = left;
			dirVecOrder[1] = down;
			dirVecOrder[2] = right;
			dirVecOrder[3] = up;
		}
		else if (deltaX > 0 && deltaY <= 0)
		{
			dirOrder[0] = Right;
			dirOrder[1] = Up;
			dirOrder[2] = Left;
			dirOrder[3] = Down;

			dirVecOrder[0] = right;
			dirVecOrder[1] = up;
			dirVecOrder[2] = left;
			dirVecOrder[3] = down;
		}
		else if (deltaX > 0 && deltaY > 0)
		{
			dirOrder[0] = Right;
			dirOrder[1] = Down;
			dirOrder[2] = Left;
			dirOrder[3] = Up;

			dirVecOrder[0] = right;
			dirVecOrder[1] = down;
			dirVecOrder[2] = left;
			dirVecOrder[3] = up;
		}
	
	} else 
	{
		if (deltaX <= 0 && deltaY <= 0) 
		{
			dirOrder[0] = Up;
			dirOrder[1] = Left;
			dirOrder[2] = Down;
			dirOrder[3] = Right;

			dirVecOrder[0] = up;
			dirVecOrder[1] = left;
			dirVecOrder[2] = down;
			dirVecOrder[3] = right;
		} 
		else if (deltaX <= 0 && deltaY > 0)
		{
			dirOrder[0] = Down;
			dirOrder[1] = Left;
			dirOrder[2] = Up;
			dirOrder[3] = Right;

			dirVecOrder[0] = down;
			dirVecOrder[1] = left;
			dirVecOrder[2] = up;
			dirVecOrder[3] = right;

		}
		else if (deltaX > 0 && deltaY <= 0)
		{
			dirOrder[0] = Up;
			dirOrder[1] = Right;
			dirOrder[2] = Down;
			dirOrder[3] = Left;

			dirVecOrder[0] = up;
			dirVecOrder[1] = right;
			dirVecOrder[2] = down;
			dirVecOrder[3] = left;
		}
		else if (deltaX > 0 && deltaY > 0)
		{
			dirOrder[0] = Down;
			dirOrder[1] = Right;
			dirOrder[2] = Up;
			dirOrder[3] = Left;

			dirVecOrder[0] = down;
			dirVecOrder[1] = right;
			dirVecOrder[2] = up;
			dirVecOrder[3] = left;
		}
	}

	for (int i=0; i<4; i++) {
		
		if (i < 2) 
		{
			oppositeDir = 2;	
		}
		else {
			oppositeDir = -2;
		}

		if (dirOrder[i] == Up) {
			if(!m_Field->IsBlockedForGhost(dirVecOrder[i].x, dirVecOrder[i].y) && m_LastDecision != dirOrder[i+oppositeDir]) 
			{
				m_LastDecision = dirOrder[i];
				SetDirection(dirOrder[i]);
				break;
			}
		}
		else
		{
			if(!m_Field->IsBlocked(dirVecOrder[i].x, dirVecOrder[i].y) && m_LastDecision != dirOrder[i+oppositeDir]) 
			{
				m_LastDecision = dirOrder[i];
				SetDirection(dirOrder[i]);
				break;
			}
		}
	}*/

}
