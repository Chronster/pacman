#pragma once

#include "BaseGhost.h"
#include "../Pacman.h"

BaseGhost::BaseGhost(void) : 
m_EatableSprite(0), m_EyesSprite(0), m_Eatable(false),  m_CornerX(0), m_CornerY(0), m_TargetX(0), m_TargetY(0), m_CurrentState(Patrol)
{
}

BaseGhost::BaseGhost(MainCharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY ) : 
m_EatableSprite(0), m_EyesSprite(0), m_Eatable(false), m_CornerX(cornerX), m_CornerY(cornerY), m_TargetX(0), m_TargetY(0), m_CurrentState(Patrol)
{
	m_PosX = xPos;
	m_PosY = yPos;
	m_PixelPosX = xPos * 16;
	m_PixelPosY = yPos * 16 - 8;
	m_Field = field;
	m_Pacman = pacman;
	m_StartPosX = xPos;
	m_StartPosY = yPos;
}

BaseGhost::~BaseGhost(void)
{
	delete m_EatableSprite;
	delete m_EyesSprite;
}

void BaseGhost::Move()
{
	if(m_Sprite == 0)
	{
		return;
	}

	if (m_PosX == m_Pacman->GetPosX() && m_PosY == m_Pacman->GetPosY())
	{
		if(m_CurrentState == Frightened && m_Alive == true && m_Pacman->isAlive()) 
		{
			m_Alive = false;
			m_Eatable = false;
			m_Pacman->IncGhostsEaten();
		} 
		
		else if(m_CurrentState != Frightened && m_Alive == true && m_Pacman->isAlive())
		{
			m_Pacman->setAlive(false);
			m_Pacman->DecLifes();
		}
	}

	if(m_CurrentState == Frightened && (System::get_time() - m_StartTime) / 1000.0f > 6.0f)
	{
		m_CurrentState = Patrol;
		m_Eatable = false;
		m_StartTime = 0;
	}

	// check if the ghost is able to change direction
		if((GetDirection() == None || ((m_PixelPosX - 8) % 16 == 0 && (m_PixelPosY - 8) % 16 == 0 && (m_Field->IsCrossing(m_PosX, m_PosY) || m_Field->IsEdge(m_PosX, m_PosY)))))
		{
			if (m_Alive == true)
			{
				
				switch(m_CurrentState)
				{
				case Patrol:
					SetTarget(m_CornerX, m_CornerY);
					MoveToTarget();
					break;

				case Persuit:
					Chase(m_Pacman->GetPosX(), m_Pacman->GetPosY());
					break;

				case Frightened:
					Escape();
					break;
				}
			}
			else 
			{
				m_CurrentState = Patrol;
				SetTarget(13, 13);
				MoveToTarget();
			}
		}

	switch(GetDirection())
	{
		case None:
			return;

		case Up:
			if(m_Field->IsBlockedForGhost(m_PosX, m_PosY - 1))
			{
				SetDirection(None);
				break;
			}

			m_PixelPosY--;

			if( ((m_PixelPosY + 8) % 16) == 0)
			{
				m_PosY = (m_PixelPosY + 8) / 16;
			}
			break;

		case Down:
			if(m_Field->IsBlocked(m_PosX, m_PosY + 1) && m_Alive == true)
			{
				SetDirection(None);
				break;
			}

			if(m_Field->IsBlockedForGhost(m_PosX, m_PosY + 1) && m_Alive == false)
			{
				SetDirection(None);
				break;
			}

			m_PixelPosY++;

			if( ((m_PixelPosY + 8) % 16) == 0)
			{
				m_PosY = (m_PixelPosY + 8) / 16;
			}
			break;

		case Left:
			if(m_Field->IsBlocked(m_PosX - 1, m_PosY))
			{
				SetDirection(None);
				break;
			}

			m_PixelPosX--;

			if(((m_PixelPosX + 8) % 16) == 0)
			{
				m_PosX = (m_PixelPosX + 8) / 16;
				if(m_PosX < 0)
				{
					m_PosX = m_Field->GetSizeX() - 1;
					m_PixelPosX = m_PosX * 16;
				}
			}
			break;

		case Right:
			if(m_Field->IsBlocked(m_PosX + 1, m_PosY))
			{
				SetDirection(None);
				break;
			}

			m_PixelPosX++;

			if( ((m_PixelPosX + 8) % 16) == 0)
			{
				m_PosX = (m_PixelPosX + 8) / 16;
				if(m_PosX >= m_Field->GetSizeX() - 1)
				{
					m_PosX = 0;
					m_PixelPosX = 0;
				}
			}
			break;
	}
}

void BaseGhost::MoveToTarget()
{
	int deltaX, deltaY;
	float dist = 0;
	float minDist = 999999;
	EDirectionType nextDir = None;

	deltaX = m_PosX - m_TargetX;
	deltaY = m_PosY - m_TargetY;

	Vec2<int> up, left, down, right, target, next;
	up = Vec2<int>(m_PosX, m_PosY-1);
	left = Vec2<int>(m_PosX-1, m_PosY);
	down = Vec2<int>(m_PosX, m_PosY+1);
	right = Vec2<int>(m_PosX+1, m_PosY);
	target = Vec2<int>(m_TargetX, m_TargetY);

	if (m_Field->InHouse(m_PosX, m_PosY) && m_Alive == false) {
		m_Alive = true;
		SetTarget(m_CornerX, m_CornerY);
		if (round%2 == 0) 
		{
			SetState(BaseGhost::EGhostState::Patrol);
		} 
		else
		{
			SetState(BaseGhost::EGhostState::Persuit);
		}
	}

	if (m_Field->InHouse(m_PosX, m_PosY) && m_Alive == true) 
	{
		if(!m_Field->IsBlocked(right.x, right.y) && m_LastDecision != Left) 
		{
			nextDir = Right;
		}

		
		if(!m_Field->IsBlocked(left.x, left.y) && m_LastDecision != Right) 
		{
			nextDir = Left;
		}
		
		if(!m_Field->IsBlockedForGhost(up.x, up.y) && m_LastDecision != Down) 
		{
			nextDir = Up;
		}
	}
	
	else 
	{
		if(!m_Field->IsBlocked(right.x, right.y) && m_LastDecision != Left) 
		{
			dist = static_cast<float>((target - right).length());
			minDist = dist;
			nextDir = Right;
		}

		if(!m_Field->IsBlocked(down.x, down.y) && m_LastDecision != Up) 
		{
			dist = static_cast<float>((target - down).length());
			if (dist <= minDist)
			{
				minDist = dist;
				nextDir = Down;
			}
		}

		if(!m_Field->IsBlockedForGhost(down.x, down.y) && m_LastDecision != Up && m_Alive == false) 
		{
			dist = static_cast<float>((target - down).length());
			if (dist <= minDist)
			{
				minDist = dist;
				nextDir = Down;
			}
		}

		if(!m_Field->IsBlocked(left.x, left.y) && m_LastDecision != Right) 
		{
			dist = static_cast<float>((target - left).length());
		
			if (dist <= minDist)
			{
				minDist = dist;
				nextDir = Left;
			}
		}

		if(!m_Field->IsBlockedForGhost(up.x, up.y) && m_LastDecision != Down) 
		{
			dist = static_cast<float>((target - up).length());
			if (dist <= minDist)
			{
				minDist = dist;
				nextDir = Up;
			}
		}
	}

	m_LastDecision = nextDir;
	SetDirection(nextDir);
}

void BaseGhost::MoveToTargetPersueMode(int posX, int posY)
{
	int oppositeDir = 2;
	float deltaX = static_cast<float>(posX - m_PosX);
	float deltaY = static_cast<float>(posY - m_PosY);

	EDirectionType dirOrder[4];
	Vec2<int> dirVecOrder[4];

	Vec2<int> up, left, down, right;
	up = Vec2<int>(m_PosX, m_PosY-1);
	left = Vec2<int>(m_PosX-1, m_PosY);
	down = Vec2<int>(m_PosX, m_PosY+1);
	right = Vec2<int>(m_PosX+1, m_PosY);

	if (m_Field->InHouse(m_PosX, m_PosY) && m_Alive == false) 
	{
		MoveToTarget();
	}
	else
	{
		if (abs(deltaX) >= abs(deltaY))
		{
			if (deltaX <= 0 && deltaY <= 0) 
			{
				dirOrder[0] = Left;
				dirOrder[1] = Up;
				dirOrder[2] = Right;
				dirOrder[3] = Down;

				dirVecOrder[0] = left;
				dirVecOrder[1] = up;
				dirVecOrder[2] = right;
				dirVecOrder[3] = down;
			} 
			else if (deltaX <= 0 && deltaY > 0)
			{
				dirOrder[0] = Left;
				dirOrder[1] = Down;
				dirOrder[2] = Right;
				dirOrder[3] = Up;

				dirVecOrder[0] = left;
				dirVecOrder[1] = down;
				dirVecOrder[2] = right;
				dirVecOrder[3] = up;
			}
			else if (deltaX > 0 && deltaY <= 0)
			{
				dirOrder[0] = Right;
				dirOrder[1] = Up;
				dirOrder[2] = Left;
				dirOrder[3] = Down;

				dirVecOrder[0] = right;
				dirVecOrder[1] = up;
				dirVecOrder[2] = left;
				dirVecOrder[3] = down;
			}
			else if (deltaX > 0 && deltaY > 0)
			{
				dirOrder[0] = Right;
				dirOrder[1] = Down;
				dirOrder[2] = Left;
				dirOrder[3] = Up;

				dirVecOrder[0] = right;
				dirVecOrder[1] = down;
				dirVecOrder[2] = left;
				dirVecOrder[3] = up;
			}
	
		} 
		
		else 
		{
			if (deltaX <= 0 && deltaY <= 0) 
			{
				dirOrder[0] = Up;
				dirOrder[1] = Left;
				dirOrder[2] = Down;
				dirOrder[3] = Right;

				dirVecOrder[0] = up;
				dirVecOrder[1] = left;
				dirVecOrder[2] = down;
				dirVecOrder[3] = right;
			} 
			else if (deltaX <= 0 && deltaY > 0)
			{
				dirOrder[0] = Down;
				dirOrder[1] = Left;
				dirOrder[2] = Up;
				dirOrder[3] = Right;

				dirVecOrder[0] = down;
				dirVecOrder[1] = left;
				dirVecOrder[2] = up;
				dirVecOrder[3] = right;

			}
			else if (deltaX > 0 && deltaY <= 0)
			{
				dirOrder[0] = Up;
				dirOrder[1] = Right;
				dirOrder[2] = Down;
				dirOrder[3] = Left;

				dirVecOrder[0] = up;
				dirVecOrder[1] = right;
				dirVecOrder[2] = down;
				dirVecOrder[3] = left;
			}
			else if (deltaX > 0 && deltaY > 0)
			{
				dirOrder[0] = Down;
				dirOrder[1] = Right;
				dirOrder[2] = Up;
				dirOrder[3] = Left;

				dirVecOrder[0] = down;
				dirVecOrder[1] = right;
				dirVecOrder[2] = up;
				dirVecOrder[3] = left;
			}
		}

		for (int i=0; i<4; i++) {
		
			if (i < 2) 
			{
				oppositeDir = 2;	
			}
			else {
				oppositeDir = -2;
			}

			if (dirOrder[i] == Up) {
				if(!m_Field->IsBlockedForGhost(dirVecOrder[i].x, dirVecOrder[i].y) && m_LastDecision != dirOrder[i+oppositeDir]) 
				{
					m_LastDecision = dirOrder[i];
					SetDirection(dirOrder[i]);
					break;
				}
			}
			else
			{
				if(!m_Field->IsBlocked(dirVecOrder[i].x, dirVecOrder[i].y) && m_LastDecision != dirOrder[i+oppositeDir]) 
				{
					m_LastDecision = dirOrder[i];
					SetDirection(dirOrder[i]);
					break;
				}
			}
		}
	}
}

void BaseGhost::Render(Canvas gc)
{
	if(!m_EatableSprite)
	{
		ResourceManager resources = XMLResourceManager::create(XMLResourceDocument("resources/resources.xml"));
		m_EatableSprite = new Sprite();
		*m_EatableSprite = Sprite::resource(gc, "ghost_eatable", resources);
	}

	if (m_Alive) {
		m_EatableSprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
		m_EatableSprite->update(10);
	}
}

void BaseGhost::RenderEyes(Canvas gc)
{
	if(!m_EyesSprite)
	{
		ResourceManager resources = XMLResourceManager::create(XMLResourceDocument("resources/resources.xml"));
		m_EyesSprite = new Sprite();
		*m_EyesSprite = Sprite::resource(gc, "ghost_eyes", resources);
	}

	m_EyesSprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
	m_EyesSprite->update(10);
}

void BaseGhost::SetTarget( int targetX,  int targetY)
{
	m_TargetX = targetX;
	m_TargetY = targetY;
}

void BaseGhost::SetState(EGhostState state)
{
	if((state != EGhostState::Frightened && m_CurrentState != EGhostState::Frightened) || m_Alive == false)
	{
		m_CurrentState = state;
		return;
	}

	if(state == EGhostState::Frightened)
	{
		m_StartTime = static_cast<int>(System::get_time());
		m_CurrentState = Frightened;

		switch(GetDirection())
		{
		case Up:
			if(SetDirection(Down))
			{
				m_LastDecision = Down;
			}
			break;
		case Down:
			if(SetDirection(Up))
			{
				m_LastDecision = Up;
			}
			break;
		case Left:
			if(SetDirection(Right))
			{
				m_LastDecision = Right;
			}
			break;
		case Right:
			if(SetDirection(Left))
			{
				m_LastDecision = Left;
			}
			break;
		}

		m_Eatable = true;
	}
}

void BaseGhost::Escape()
{
	srand ( static_cast<unsigned int>(time(NULL)) );
	int newDirection = rand() % 4 + 1;
	bool directionSet = false;

	while(!directionSet)
	{
		// m_LastDecision- Variable kann von Move-Methode �bernommen werden, damit die KI nicht den selben Weg zur�ck geht, den sie gekommen ist
		if(static_cast<EDirectionType>(newDirection) != m_LastDecision) //weg zur�ck nicht nehmen
		{
			if(SetDirection(static_cast<EDirectionType>(newDirection))) //neuen weg testen
			{
				m_LastDecision = static_cast<EDirectionType>(newDirection);
				directionSet = true;
			}
		}

		newDirection = newDirection % 4 + 1;
	}
}

void BaseGhost::ResetPosition()
{
	m_PosX = m_StartPosX;
	m_PosY = m_StartPosY;
	m_PixelPosX = m_PosX * 16;
	m_PixelPosY = m_PosY * 16 - 8;
	m_Alive = true;
	m_CurrentState = Patrol;
	m_Eatable = false;
	m_StartTime = 0;
}