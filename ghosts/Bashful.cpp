//Bashful = grün
// Nickname: Inky
// Bashful has the most complicated AI of all. When
//the ghost is not patrolling its home corner
//(southeast), Bashful considers two things
// Shadow's location
// the location two grid spaces ahead of Pac-Man.
// Bashful draws a line from Shadow to the spot two
//squares in front of Pac-Man, and extends that line
//twice as far. Therefore, if Bashful is alongside
//Shadow when they are behind Pac-Man, Bashful
//will usually follow Shadow the whole time. But if
//Bashful is in front of Pac-Man when Shadow is
//behind him, Bashful tends to want to move away
//from Pac-Man (in reality, to a point very far ahead
//of Pac-Man).

#include "Bashful.h"

Bashful::Bashful(MainCharacter* pacman, ACharacter* shadow, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY) : BaseGhost(pacman, field, xPos, yPos, cornerX, cornerY)
{
	m_Shadow = shadow;
}

Bashful::~Bashful(void)
{
	delete m_Sprite;
}

void Bashful::Render(Canvas gc)
{
	if(!m_Eatable)
	{
		if(!m_Sprite)
		{
			ResourceManager resources = XMLResourceManager::create(XMLResourceDocument("resources/resources.xml"));
			m_Sprite = new Sprite();
			*m_Sprite = Sprite::resource(gc, "bashful", resources);
		}

		if (m_Alive == true) 
		{
			m_Sprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
			m_Sprite->update(10);
		}

		BaseGhost::RenderEyes(gc);
	}
	else
	{
		BaseGhost::Render(gc);
	}
}

void Bashful::Chase(int pacmanX, int pacmanY)
{
	Vec2<int> shadowPosition = Vec2<int>(m_Shadow->GetPosX(), m_Shadow->GetPosY());
	EDirectionType lookDirection = m_Pacman->GetViewDirection();
	Vec2<int> targetPosition;

	if (lookDirection == Up)
	{
		targetPosition = 2 * (Vec2<int>(pacmanX, pacmanY-2) - shadowPosition);
	} 
	else if (lookDirection == Right)
	{
		targetPosition = 2 * (Vec2<int>(pacmanX+2, pacmanY) - shadowPosition);
	}
	else if (lookDirection == Down)
	{
		targetPosition = 2 * (Vec2<int>(pacmanX, pacmanY+2) - shadowPosition);
	}
	else if (lookDirection == Left)
	{
		targetPosition = 2 * (Vec2<int>(pacmanX-2, pacmanY) - shadowPosition);
	}

	MoveToTargetPersueMode(targetPosition.x, targetPosition.y);

}