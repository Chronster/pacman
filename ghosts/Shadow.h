#pragma once

#include "BaseGhost.h"

class Shadow : public BaseGhost
{
public:
	Shadow(MainCharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY);
	~Shadow(void);
	
	virtual void Render(Canvas gc);
	virtual void Chase(int pacmanX, int pacmanY);
};

