//Pokey = gelb
//� Nickname: Clyde
//� Pokey is the loner of the group, usually off doing
//his own thing when he's not patrolling the
//southwest corner of the maze. His behavior is
//very random.
//� Pokey has two basic AIs
//� one for when he's far from Pac-Man (beyond 8
//grid spaces):
//� Pokey behaves very much like Blinky,
//trying to move to Pac-Man's exact location
//� one for when he is near to Pac-Man:
//� Pokey goes to his home corner in the
//bottom left of the maze

#include "Pokey.h"

Pokey::Pokey(MainCharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY) : BaseGhost(pacman, field, xPos, yPos, cornerX, cornerY)
{
}

Pokey::~Pokey(void)
{
	delete m_Sprite;
}

void Pokey::Render(Canvas gc)
{
	if(!m_Eatable)
	{
		if(!m_Sprite)
		{
			ResourceManager resources = XMLResourceManager::create(XMLResourceDocument("resources/resources.xml"));
			m_Sprite = new Sprite();
			*m_Sprite = Sprite::resource(gc, "pokey", resources);
		}

		if (m_Alive == true) 
		{
			m_Sprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
			m_Sprite->update(10);
		}

		BaseGhost::RenderEyes(gc);
	}
	else
	{
		BaseGhost::Render(gc);
	}
}

void Pokey::Chase(int pacmanX, int pacmanY)
{
	float distance = abs((Vec2<float>(static_cast<float>(pacmanX), static_cast<float>(pacmanY)) - Vec2<float>(static_cast<float>(m_PosX), static_cast<float>(m_PosY))).length());
	if (distance > 8.0f) 
	{
		MoveToTargetPersueMode(pacmanX, pacmanY);
	}
	else
	{
		MoveToTarget();
	}
}