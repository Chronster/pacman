#pragma once

#include "precomp.h"
#include "Field.h"
#include "pacman\MainCharacter.h"
#include "ghosts\BaseGhost.h"
#include "ghosts\Shadow.h"
#include "ghosts\Speedy.h"
#include "ghosts\Bashful.h"
#include "ghosts\Pokey.h"
#include "Score.h"
#include "Endscene.h"

static unsigned short round=0;

class Pacman
{
public:
	Pacman(void);
	~Pacman(void);
	int start(const std::vector<string> &args);

private:
	Field* m_Field;
	MainCharacter* m_Pacman;
	Shadow* m_Shadow;
	Speedy* m_Speedy;
	Bashful* m_Bashful;
	Pokey* m_Pokey;
	Score* m_Score;
	Endscene* m_Endscene;
	Sprite *resArray[5];

	void on_input_up(const InputEvent &key);
	
	void on_window_close();
	bool quit;
};

