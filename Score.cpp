#include "Score.h"
#include <sstream>

Score::Score(Canvas gc, Field* field, MainCharacter* pacman)
{
	m_Gc = gc;
	m_Field = field;
	m_Score = 0;
	m_StandardFont = Font::Font(m_Gc,"arial", 16);
	m_Pacman = pacman;
	ResourceManager resources = XMLResourceManager::create(XMLResourceDocument("resources/resources.xml"));
	m_LifeSprite = new Sprite();
	*m_LifeSprite = Sprite::resource(m_Gc, "life", resources);
}

Score::~Score(void)
{
	delete m_LifeSprite;
}

void Score::DrawScore(void)
{
	m_Score = m_Field->GetDotsEaten()*10 + m_Field->GetBigDotsEaten()*50 + m_Pacman->GetGhostsEaten()*250;
	std::stringstream ss;			//create a stringstream
	ss << m_Score;					//add score to the stream
	std::string text = ss.str();

	float scale = 0.5; 
	int gc_width = m_Gc.get_width();
	int gc_height = m_Gc.get_height();
    Size text_size = m_StandardFont.get_text_size(m_Gc, text);
	
	int xpos = (gc_width - text_size.width);
	int ypos = gc_height -3;
	m_StandardFont.draw_text(m_Gc, xpos, ypos, text, Colorf(0.8f, 0.3f, 0.0f, 1.0f));
	
	for (int i=0; i<m_Pacman->GetLifes(); i++)
	{
		m_LifeSprite->set_scale(scale, scale);
		m_LifeSprite->update(10);
		m_LifeSprite->draw(m_Gc, i * (32.0f * scale), gc_height - (32.0f * scale));
	}
}

void Score::Add(int points) 
{
	m_Score += points;
}

unsigned int* Score::GetScore()
{
	return &m_Score;
}