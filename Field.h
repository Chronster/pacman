#pragma once

#include "precomp.h"
#include <vector>
#include <map>

using namespace std;

class Field
{
public:
	Field(Canvas gc, ResourceManager resources);
	~Field(void);
	void LoadField();
	void DrawField();
	bool IsBlocked(const  int x, const int y) const;
	bool IsBlockedForGhost(const int x, const int y) const;
	char GetField(const  int x, const int y) const; 
	void EatField(const  int x, const int y);
	bool IsCrossing(const  int x, const int y) const;
	bool IsEdge(const int x, const int y) const;
	int GetSizeX() const;
	int GetSizeY() const;
	bool GetBigDotEaten();
	unsigned int GetDotsEaten() const;
	unsigned int GetBigDotsEaten() const;
	bool InHouse(const int x, const int y) const;
private:
	int m_SizeX, m_SizeY;
	vector<char*> m_Field;
	Canvas m_Gc;
	ResourceManager m_Resources;
	map<char, Sprite> m_FieldTiles;
	unsigned int m_DotsEaten;
	unsigned int m_BigDotsEaten;
	bool m_BigDotEaten;
};

