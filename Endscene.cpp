#include "Endscene.h"
#include <sstream>

Endscene::Endscene(Canvas gc, int *score)
{
	m_Gc = gc;
	m_Score = score;
	m_GameOverFont = Font::Font(m_Gc,"arial", 40); 
	m_ScoreFont = Font::Font(m_Gc,"arial", 32);
}

Endscene::~Endscene(void)
{
}

void Endscene::Render(void)
{
	int gc_width = m_Gc.get_width();
	int gc_height = m_Gc.get_height();

	std::stringstream ss;						//create a stringstream
	ss << "Score:  " << *m_Score << " points";	//add score to the stream
	std::string scoreStr = ss.str();
	std::string gameOverStr = "Game Over";

    Size score_size = m_ScoreFont.get_text_size(m_Gc, scoreStr);
	Size gameOver_size = m_GameOverFont.get_text_size(m_Gc, gameOverStr);
	
	int xposGameOver = (gc_width - gameOver_size.width)/2;
	int yposGameOver = gc_height/3;
	int xposScore = (gc_width - score_size.width)/2;
	int yposScore = (gc_height/3)*2 - score_size.height/2;
	m_GameOverFont.draw_text(m_Gc, xposGameOver, yposGameOver, gameOverStr, Colorf(0.8f, 0.7f, 0.0f, 1.0f));
	m_ScoreFont.draw_text(m_Gc, xposScore, yposScore, scoreStr, Colorf(0.8f, 0.7f, 0.0f, 1.0f));
}