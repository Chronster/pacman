#pragma once

#include "precomp.h"
#include "Score.h"

class Endscene
{
public:
	Endscene(Canvas gc, int *score);
	~Endscene(void);
	void Render();

private:
	int *m_Score;
	Canvas m_Gc;
	Font m_ScoreFont;
	Font m_GameOverFont;
};
