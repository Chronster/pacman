#pragma once
#include "../ACharacter.h"
#include "../precomp.h"

#define BUFFERTIME  32

class MainCharacter : public ACharacter
{
public:
	MainCharacter(Sprite* spriteUp, Sprite* spriteRight, Sprite* spriteDown, Sprite* spriteLeft, Sprite* spriteDead, InputDevice keyboard, Field* field);
	~MainCharacter(void);
	void Update();
	void Render(Canvas gc);
	int GetGhostsEaten();
	void IncGhostsEaten();
	void DecLifes();
	int GetLifes();
	void ResetPosition();
	bool HasDeathAnimationEnded();

private:
	Sprite* m_SpriteUp;
	Sprite* m_SpriteRight;
	Sprite* m_SpriteDown;
	Sprite* m_SpriteLeft;
	Sprite* m_SpriteDead;
	InputDevice m_Keyboard;
	int m_PressedKeyBuffer;
	short m_CurrentBufferStep;
	int m_GhostsEaten;
	int m_Lives;

	virtual void Move();
	virtual bool SetDirection(EDirectionType);
};