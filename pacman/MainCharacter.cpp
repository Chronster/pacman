#include "MainCharacter.h"

MainCharacter::MainCharacter(Sprite* spriteUp, Sprite* spriteRight, Sprite* spriteDown, Sprite* spriteLeft, Sprite* spriteDead, InputDevice keyboard, Field* field)
{		
	m_Sprite = spriteDown;
	m_SpriteUp = spriteUp;
	m_SpriteRight = spriteRight;
	m_SpriteDown = spriteDown;
	m_SpriteLeft = spriteLeft;
	m_SpriteDead = spriteDead;
	m_Keyboard = keyboard;
	m_Field = field;
	m_PressedKeyBuffer = 0;
	m_CurrentBufferStep = 0;
	m_PosX = 13;
	m_PosY = 23;
	m_PixelPosX = m_PosX * 16;
	m_PixelPosY = m_PosY * 16 - 8;
	m_GhostsEaten = 0;
	m_Lives = 3;
}

MainCharacter::~MainCharacter(void)
{
}

void MainCharacter::Update()
{
	if(m_CurrentBufferStep > 0)
	{
		--m_CurrentBufferStep;
	}
	else
	{
		m_PressedKeyBuffer = 0;
	}

	if(m_Keyboard.get_keycode(keycode_w) || m_Keyboard.get_keycode(keycode_up))
	{
		m_PressedKeyBuffer = keycode_w;
		m_CurrentBufferStep = BUFFERTIME;
	}
	else if(m_Keyboard.get_keycode(keycode_a) || m_Keyboard.get_keycode(keycode_left))
	{
		m_PressedKeyBuffer = keycode_a;
		m_CurrentBufferStep = BUFFERTIME;
	}	
	else if(m_Keyboard.get_keycode(keycode_s) || m_Keyboard.get_keycode(keycode_down))
	{
		m_PressedKeyBuffer = keycode_s;
		m_CurrentBufferStep = BUFFERTIME;
	}	
	else if(m_Keyboard.get_keycode(keycode_d) || m_Keyboard.get_keycode(keycode_right))
	{
		m_PressedKeyBuffer = keycode_d;
		m_CurrentBufferStep = BUFFERTIME;
	}

	if(m_CurrentBufferStep > 0)
	{
		if(m_PressedKeyBuffer == keycode_w && !m_Field->IsBlocked(m_PosX, m_PosY - 1))
		{
			if(SetDirection(Up))
			{
				m_PressedKeyBuffer = 0;
				m_CurrentBufferStep = 0;
			}
		}	
		else if(m_PressedKeyBuffer == keycode_a && !m_Field->IsBlocked(m_PosX - 1, m_PosY))
		{
			if(SetDirection(Left))
			{
				m_PressedKeyBuffer = 0;
				m_CurrentBufferStep = 0;
			}
		}	
		else if(m_PressedKeyBuffer == keycode_s && !m_Field->IsBlocked(m_PosX, m_PosY + 1))
		{
			if(SetDirection(Down))
			{
				m_PressedKeyBuffer = 0;
				m_CurrentBufferStep = 0;
			}
		}	
		else if(m_PressedKeyBuffer == keycode_d && !m_Field->IsBlocked(m_PosX + 1, m_PosY))
		{
			if(SetDirection(Right))
			{
				m_PressedKeyBuffer = 0;
				m_CurrentBufferStep = 0;
			}
		}
	}

	ACharacter::Update();
}

void MainCharacter::Move()
{

	if (m_Alive) 
	{
		ACharacter::Move();

		char fieldChar = m_Field->GetField(m_PosX, m_PosY);

		if(fieldChar == '.' || fieldChar == '*')
		{
			m_Field->EatField(m_PosX, m_PosY);
		}
	} 
	else
	{
		m_Sprite = m_SpriteDead;
	}
}

bool MainCharacter::SetDirection(EDirectionType directionType)
{
	bool value = false;

	if(!m_Alive)
	{
		value = ACharacter::SetDirection(None);
	} else {
		value = ACharacter::SetDirection(directionType);

		if(value)
		{
			switch(GetDirection())
			{
			case Up:
				m_Sprite = m_SpriteUp;
				break;

			case Right:
				m_Sprite = m_SpriteRight;
				break;

			case Down:
				m_Sprite = m_SpriteDown;
				break;

			case Left:
				m_Sprite = m_SpriteLeft;
				break;
			}
		}
	}

	return value;
}

int MainCharacter::GetGhostsEaten()
{
	return m_GhostsEaten;
}

void MainCharacter::IncGhostsEaten()
{
	++m_GhostsEaten;
}

void MainCharacter::DecLifes() 
{
	--m_Lives;
}

int MainCharacter::GetLifes()
{
	return m_Lives;
}

void MainCharacter::ResetPosition()
{
	m_SpriteDead->restart();
	m_SpriteDown->restart();
	m_Sprite = m_SpriteDown;
	m_PressedKeyBuffer = 0;
	m_CurrentBufferStep = 0;
	m_PosX = 13;
	m_PosY = 23;
	m_PixelPosX = m_PosX * 16;
	m_PixelPosY = m_PosY * 16 - 8;
	SetDirection(None);
	m_Alive = true;
}

bool MainCharacter::HasDeathAnimationEnded()
{
	if(m_Sprite->is_finished() && !m_Alive)
	{
		return true;
	}

	return false;
}

void MainCharacter::Render(Canvas gc)
{
	if(GetDirection() != None || !m_Alive)
	{
		m_Sprite->update(10);
	}

	m_Sprite->draw(gc, (float)m_PixelPosX, (float)m_PixelPosY);
}