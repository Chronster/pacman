#pragma once

#include "precomp.h"
#include "Field.h"

enum EDirectionType 
{
	None = 0,
	Up = 1,
	Right = 2,
	Down = 3,
	Left = 4
};

class ACharacter
{
public:

	ACharacter(void);
	~ACharacter(void);

	virtual void Update();
	virtual void Render(Canvas gc) = 0;

	virtual bool SetDirection(EDirectionType);
	EDirectionType GetDirection() const;
	EDirectionType GetViewDirection() const;
	void setAlive(bool);

	short GetPosX();
	short GetPosY();
	bool isAlive();

protected:
	short m_PosX;
	short m_PosY;

	Sprite* m_Sprite;

	int m_PixelPosX;
	int m_PixelPosY;
	
	Field* m_Field;

	bool m_Alive;

	virtual void Move();
private:
	EDirectionType m_DirectionType;
	EDirectionType m_ViewDirection;
};